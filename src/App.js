import Questions from "./components/Questions/Index";

function App() {
  return (
    <div>
      <Questions />
    </div>
  );
}

export default App;
