import React, { Component, Fragment } from 'react'
import MultipleChoice from '../MultipleChoice/Index'
import FillBlank from '../FillBlank/Index'
import { Button, Container, Typography } from '@material-ui/core'
import { fetchQuestions } from "../../store/actions/questions"; 
import {connect} from 'react-redux'
 class Questions extends Component {
     checkReuslt = ()=>{
        let total = 0;
        for( let i of this.props.DanhSachDapAn){
            if(i.answer.exact){
                total +=1;
            }
        }
        alert ("Tổng số điểm là:"+total);
     }
    render() {
        return (
            <Container maxWidth='lg'>
                <Typography component = 'h2' align='center' variant ='h4' color ="textSecondary">
                    Danh Sách Câu Hỏi
                </Typography>
                <Fragment>
                    <MultipleChoice/>
                    <FillBlank/>
                </Fragment>
                <Button onClick={this.checkReuslt}>Chấm Điểm</Button>
            </Container>
        )
    }
    componentDidMount(){
        this.props.dispatch(fetchQuestions);
     };
};
const mapStateToProps = (state)=>{
    return {
        DanhSachDapAn: state.questions.DanhSachDapAn,
    }
}
export default connect(mapStateToProps)(Questions)
