import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
class FillBlank extends Component {
  timer = "";
  handleChange = (event, item, answer) => {
    if (this.timer) {
      clearTimeout(this.timer);
    }

    const cloneCheckInput = { ...this.state.checkInput };
    cloneCheckInput[event.target.name] = event.target.value;
    this.setState(
      {
        checkInput: cloneCheckInput,
      },
      () => {
        const objectAnswer = {
          question: {
            item,
            answer: this.state.checkInput[answer],
          },
        };
        this.props.dispatch(createAction(actionType.ADD_ANSWER, objectAnswer));
      }
    );
  };
  constructor(props) {
    super(props);
    this.state = {
      checkInput: {},
    };
  }
  render() {
    console.log(this.props.questions);
    return (
      <div>
        {Object.keys(this.props.questions).length
          ? this.props.questions
              .filter((item) => {
                return item.questionType === 2;
              })
              .map((item) => {
                return (
                  <div key={item.id}>
                    <Typography
                      component="h2"
                      align="left"
                      variant="h6"
                      color="textPrimary"
                    >
                      Câu hỏi {item.id}: {item.content}
                    </Typography>
                    <TextField
                      onChange={(event) =>
                        this.handleChange(event, item, "question" + item.id)
                      }
                      style={{ margin: 8 }}
                      margin="normal"
                      name={"question" + item.id}
                      value={this.state.checkInput["question" + item.id]}
                    />
                  </div>
                );
              })
          : ""}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    questions: state.questions.questions,
  };
};
export default connect(mapStateToProps)(FillBlank);
