import React, { Component } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
class MultipleChoice extends Component {
  handleChange = (event, item, answer) => {
    const cloneCheckInput = { ...this.state.checkInput };
    cloneCheckInput[event.target.name] = event.target.value;
    this.setState(
      {
        checkInput: cloneCheckInput,
      },
      () => {
        const objectAnswer = {
          question: {
            item,
            answer: this.state.checkInput[answer],
          },
        };
        this.props.dispatch(createAction(actionType.ADD_ANSWER, objectAnswer));
      }
    );
  };

  constructor(props) {
    super(props);
    this.state = {
      checkInput: {},
    };
  }
  render() {
    return (
      <div>
        {Object.keys(this.props.questions).length
          ? this.props.questions
              .filter((item) => {
                return item.questionType === 1;
              })
              .map((item) => {
                return (
                  <FormControl
                    key={item.id}
                    style={{ display: "block" }}
                    component="fieldset"
                  >
                    <Typography
                      component="h2"
                      align="left"
                      variant="h6"
                      color="textPrimary"
                    >
                      Câu hỏi {item.id}: {item.content}
                    </Typography>
                    <RadioGroup
                      aria-label="gender"
                      name={"question" + item.id}
                      value={this.state.checkInput["question" + item.id]}
                    >
                      {item.answers.map((answers) => {
                        return (
                          <FormControlLabel
                            onChange={(event) =>
                              this.handleChange(
                                event,
                                item,
                                "question" + item.id
                              )
                            }
                            key={answers.id}
                            value={answers.id}
                            control={<Radio />}
                            label={answers.content}
                          />
                        );
                      })}
                    </RadioGroup>
                  </FormControl>
                );
              })
          : ""}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    questions: state.questions.questions,
  };
};
export default connect(mapStateToProps)(MultipleChoice);
