import { actionType } from "../actions/type";

const initialState = {
  questions: {},
  DanhSachDapAn: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_QUESTIONS:
      state.questions = action.payload;
      return { ...state };
    case actionType.ADD_ANSWER:
      const clonecDanhSachDapAn = [...state.DanhSachDapAn];
      const foundIndex = clonecDanhSachDapAn.findIndex((item) => {
        return item.questionId === action.payload.question.item.id;
      });
      let foundIdAnswer = 0;
      let isTrue = false;
      if (action.payload.question.item.questionType === 1) {
        const foundAnswer = action.payload.question.item.answers.findIndex(
          (item) => {
            return item.id === action.payload.question.answer;
          }
        );
        foundIdAnswer = foundAnswer;
      } else {
        if (
          action.payload.question.item.answers[0].content.toLowerCase() ===
          action.payload.question.answer.toLowerCase()
        ) {
          isTrue = true;
        }
      }

      if (foundIndex === -1) {
        if (action.payload.question.item.questionType === 1) {
          const data = {
            questionId: action.payload.question.item.id,
            answer: {
              content:
                action.payload.question.item.answers[foundIdAnswer].content,
              exact: action.payload.question.item.answers[foundIdAnswer].exact,
            },
          };
          clonecDanhSachDapAn.push(data);
        } else {
          const data = {
            questionId: action.payload.question.item.id,
            answer: {
              content: action.payload.question.answer,
              exact: isTrue,
            },
          };
          clonecDanhSachDapAn.push(data);
        }
      } else {
        if (action.payload.question.item.questionType === 1) {
          clonecDanhSachDapAn[foundIndex].answer.content =
            action.payload.question.item.answers[foundIdAnswer].content;
          clonecDanhSachDapAn[foundIndex].answer.exact =
            action.payload.question.item.answers[foundIdAnswer].exact;
        } else {
          clonecDanhSachDapAn[foundIndex].answer.content =
            action.payload.question.answer;
          clonecDanhSachDapAn[foundIndex].answer.exact = isTrue;
        }
      }
      state.DanhSachDapAn = clonecDanhSachDapAn;
      return { ...state };
    default:
      return state;
  }
};
export default reducer;
