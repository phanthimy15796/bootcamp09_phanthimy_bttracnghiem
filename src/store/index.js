import {combineReducers,createStore,applyMiddleware,compose} from 'redux';
import thunk from 'redux-thunk';
import questions from "./reducers/questions"
const rootReducer = combineReducers({
    questions,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, 
    composeEnhancers(
        applyMiddleware(thunk),
    )
    
   );

   export default store;